from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth

def signup(request):
    if request.method == 'POST':
        if request.POST['password'] == request.POST['cnpass']:
            try:
                user = User.objects.get(username = request.POST['username'])
                return render(request,'signup.html',{'error':'user already exists'} )
            except User.DoesNotExist:
                user = User.objects.create_user(request.POST['username'],password = request.POST['password'],first_name= request.POST['firstname'],last_name= request.POST['lastname'],email= request.POST['email'])
                auth.login(request,user)
                return redirect('home')
        else:
            return render(request,"signup.html",{"error":"Password and Confirm passwords does not matched"})
    else:
        return render(request,'signup.html')

def login(request):
    if request.method == 'POST':
        user = auth.authenticate(username=request.POST['username'],password=request.POST['password'])
        if user is not None:
            auth.login(request,user)
            return redirect('home')
        else:
            return render(request,'login.html',{'error':'Wrong User name or password!'})
    else:
        return render(request,'login.html')

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('home')
