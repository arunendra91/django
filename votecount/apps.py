from django.apps import AppConfig


class VotecountConfig(AppConfig):
    name = 'votecount'
