from django.db import models
from django.contrib.auth.models import User
class Product(models.Model):
    title  = models.CharField(max_length=255)
    pub_date = models.DateTimeField()
    body=models.TextField()
    url=models.CharField(max_length=255)
    image=models.ImageField(upload_to='images/')
    icon=models.ImageField(upload_to='images/')
    total_votes=models.IntegerField(default=1)
    total_down_votes=models.IntegerField(default=0)
    hunter=models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def formatted_date(self):
        return self.pub_date.strftime("%b %e, %Y")
