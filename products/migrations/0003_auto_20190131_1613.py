# Generated by Django 2.0.2 on 2019-01-31 10:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_product_total_down_votes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='total_down_votes',
            field=models.IntegerField(default=0),
        ),
    ]
