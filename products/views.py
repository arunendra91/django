from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Product
from votecount.models import Votecount
from django.utils import timezone
from django.contrib import messages

def home(request):
    pmodels = Product.objects
    return render(request,'home.html',{'pmodels':pmodels})

@login_required
def create(request):
    if request.method == 'POST':
        if request.POST['title'] and  request.POST['url'] and  request.POST['body'] and  request.FILES['icon'] and  request.FILES['image']:
            pmodel = Product()
            pmodel.title = request.POST['title']
            if request.POST['url'].startswith('http://') or request.POST['url'].startswith('https://'):
                pmodel.url = request.POST['url']
            else:
                pmodel.url = 'http://' + request.POST['url']

            pmodel.icon = request.FILES['icon']
            pmodel.image = request.FILES['image']
            pmodel.body = request.POST['body']
            pmodel.pub_date = timezone.datetime.now()
            pmodel.hunter = request.user
            pmodel.save()
            return redirect('/products/'+str(pmodel.id))

        else:
            return render(request,'create.html',{'error':'All fields are required!'})
    else:
        return render(request,'create.html')


def details(request,product_id):
    pmodel = get_object_or_404(Product,pk=product_id)
    return render(request,'details.html',{'pmodel':pmodel})

def upvote(request,product_id):
    if request.method == 'POST':
        pmodel = get_object_or_404(Product,pk=product_id)
        try:
            votecountModel = Votecount.objects.get(product_id=pmodel.id)
            if votecountModel.type == 2:
                pmodel.total_votes += 1
                pmodel.total_down_votes -= 1
                pmodel.save()
                votecountModel.type=1
                votecountModel.save()
                messages.info(request, 'You have upvoted.')
            else:
                messages.warning(request, 'You have already voted.')
            return redirect("/products/" + str(pmodel.id))
        except Votecount.DoesNotExist:
            pmodel.total_votes += 1
            pmodel.save()
            votecountModel.product_id=product_id
            votecountModel.user_id=pmodel.hunter
            votecountModel.type=1
            votecountModel.save()
            messages.info(request, 'You have upvoted.')
            return redirect("/products/" + str(pmodel.id))


def downvote(request,product_id):
    if request.method == 'POST':
        pmodel = get_object_or_404(Product,pk=product_id)
        try:
            votecountModel = Votecount.objects.get(product_id=pmodel.id)
            if votecountModel.type==1:
                pmodel.total_votes -= 1
                pmodel.total_down_votes += 1
                pmodel.save()
                votecountModel.type=2
                votecountModel.save()
                messages.info(request, 'You have downvoted.')
            else:
                messages.warning(request, 'You have already voted.')
            return redirect("/products/" + str(pmodel.id))
        except Votecount.DoesNotExist:
            pmodel.total_down_votes += 1
            pmodel.save()
            votecountModel.product_id=product_id
            votecountModel.user_id=pmodel.hunter
            votecountModel.type=2
            votecountModel.save()
            messages.add_message(request, messages.INFO, 'You have downvoted.')
            return redirect("/products/" + str(pmodel.id))
