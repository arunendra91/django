Install Python :

Step 1:
Download python from here : 
https://www.python.org/downloads/

Install it in specific directory c:\python

Step 2: check python.exe file it should be C:\Python\Python37-32\python.exe copy this path

Step 3 : Paste the above path in system environment variables .

Run : python --version from command prompt 

Done!
=========

Install django :

Step 1:  run this command : python -m pip install django==2.0.2

It will install under C:\Python\Python37-32\Scripts

Step 2 : copy this location and paste in the environment variables.

========== 


create project : run command :  django-admin.py startproject wordcount

Note : make sure run cmd as admin

Run project :

C:\Python\projects\wordcount >> python manage.py runserver



create virtual environment
install in project: 

Step 1:
C:\Python\projects\wordcount:  python -m pip install virtualenv
Step 2:
Create virtual env : virtualenv testenv

Use virtual env project
Step 1:

a) activate env : 

	testenv/Scripts >>  source activate or C:\Python\portfolio\Scripts>activate.bat

b)deactivate env : 

	testenv/Scripts >>deactivate 

Step 2:

create virtual project :

 C:\Python\>> django-admin.py startproject envtestproject

Step 3: Install django in this project 


 C:\Python\>> pip3 install django==2.0.2

Step 4 : run server 

 C:\Python\>> python manage.py runserver

Step 5 : Create apps (Module) :

 C:\Python\envtestproject>> python manage.py startapp jobs


Step 5 : Make migration :

C:\Python\envtestproject>python manage.py makemigrations

Step 6 : Apply migration :

C:\Python\envtestproject>> python manage.py migrate

to install Pillo
C:\Python\portfolio-project : python -m pip3 install Pillow==5.0.0 or easy_install pillow


create superuser
C:\Python\portfolio-project : python manage.py createsuperuser
username: vinod
password : arun@123



postgresql password: admin


collect static files

step 1: in settings.py
STATICFILES_DIRS = [
  os.path.join(BASE_DIR, 'envtestproject/static/')
]
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

step 2: execute command
C:\Python\portfolio-project : python manage.py collectstatic

call static file in html   
step 1: 
{% load staticfiles %}
step 2 for linking
{% static 'admin/img/icon-calendar.svg' %}

Create requirements file :
Step -1 :
(myenv) c:\Python\envtestproject>pip freeze > requirements.txt

Step -2 :
(myenv) c:\Python\envtestproject>pip install -r requirements.txt
